function defaults(obj, defaultProps){

    if (typeof(obj) == 'object' && typeof(defaultProps)== 'object') {
        
        for (const defaultKey in defaultProps) {
            let flag = 0;
            for (const objKey in obj) {
                if (defaultKey == objKey) {
                    flag = 1;
                }
            }
    
            if (flag == 0) {
                obj[defaultKey] = defaultProps[defaultKey];
            }
        }
    
        return obj;
    } else {
        return ;
    }
}

module.exports = defaults;
function invert(obj) {
    const swappedObject = {};
    const tempObject = {};

    if (typeof(obj) == 'object' && obj != null) {
        for (const key in obj) {
            tempObject[obj[key]] = key;
        }

        for (const key in tempObject) {
            swappedObject[key] = tempObject[key];
        }
    } else {
        return ;
    }
    
    return swappedObject;
}

module.exports = invert;
function keys(obj) {
    let keysList = [];
    if (typeof(obj) == 'object' && obj != null) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                keysList.push(key);
            }
        }
    } else {
        return ;
    }
    
    
    return keysList;
}

module.exports = keys;
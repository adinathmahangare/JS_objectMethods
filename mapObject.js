function mapObject(obj, cb) {
    let newObject = {};

    if (typeof(obj) == 'object' && obj != null) {
        for (const key in obj) {
            newObject[key] = cb(key);
        }
        return newObject;
    } else {
        return ;
    }
    
}

module.exports = mapObject;
function pairs(obj) {
    let pairList = [];
    for (const key in obj) {
        pairList.push([key, obj[key]]);
    }

    return pairList;
}

module.exports = pairs;
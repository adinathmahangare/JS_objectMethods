const testObject = require('../testObject');
const defaults = require('../defaults');

function testDefaults() {

    let defaultPropertiesObject = {
        name: "Adinath",
        age: 34,
        location: "Pune",
        isStudent: false,
        Designation: "Softeware Developer"
    };

    let resultObject = {};

    resultObject = defaults(testObject, defaultPropertiesObject);

    console.log(resultObject);
}

testDefaults();
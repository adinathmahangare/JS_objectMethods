const testObject = require('../testObject');
const mapObject = require('../mapObject');

//testing mapObject function which each value in object and returns its data type
function testMapObject() {
    let resultObject = {};
    resultObject = mapObject(testObject, (key) => typeof(testObject[key]));
    console.log(resultObject);
}

testMapObject();